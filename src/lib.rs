/// Converts a markdown string to a groff/troff string.
///
/// * `title` is the name of the program. It's typically all-uppercase.
///
/// * `section` is the numeric section of man pages. Usually `1`.
///
/// The conversion is very rough. HTML fragments are merely stripped from tags.
/// GitHub tables extension is not supported.
#[must_use]
pub fn convert(markdown_markup: &str, title: &str, section: u8) -> String {
    use pulldown_cmark::Event::*;
    use pulldown_cmark::Tag::*;
    use pulldown_cmark::{Options, Parser, TagEnd};
    use pulldown_cmark::BlockQuoteKind;

    let mut options = Options::empty();
    options.insert(Options::ENABLE_STRIKETHROUGH);
    options.insert(Options::ENABLE_TABLES);
    options.insert(Options::ENABLE_GFM);
    options.insert(Options::ENABLE_DEFINITION_LIST);
    let parser = Parser::new_ext(markdown_markup, options);

    let mut out = Rough {
        out: String::new(),
        in_quotes: false,
        unclosed_table_cell: false,
        bold_level: 0,
        italic_level: 0,
    };
    out.title(title, section);

    let mut links = Vec::new();
    let mut images = Vec::new();
    let mut min_header_level = 999;
    let mut link_ref_num = 1;
    let mut list_item_num = None;
    let mut in_list = false;
    let mut first_para_in_list = false;

    fn flush_links(out: &mut Rough, links: &mut Vec<(String, String, String)>) {
        if links.is_empty() {
            return;
        }

        out.empty_line();
        for (n, url, title) in links.drain(..) {
            out.text(&format!("{n} {url} {title}"));
            out.line_break();
        }
    }

    let mut html_state = TagStrip {
        in_comment: false, in_tag: false, in_arg: None,

    };

    let mut url_stack = Vec::new();
    for event in parser {
        match event {
            Rule => {
                html_state.reset();
                out.centered("----")
            },
            Html(markup) | DisplayMath(markup) => {
                out.ensure_line_start();
                out.text(&html_state.strip_tags(&markup));
            },
            InlineHtml(markup) | InlineMath(markup) => {
                html_state.reset();
                out.text(&html_state.strip_tags(&markup));
            },
            TaskListMarker(checked) => out.text(if checked {"[x]"} else {"[ ]"}),
            Start(Heading { level: n, .. }) => {
                let n = n as u32;
                flush_links(&mut out, &mut links);

                if n < min_header_level {
                    min_header_level = n;
                }
                out.section_start(n + 1 - min_header_level);
            },
            End(TagEnd::Heading(n)) => out.section_end((n as u32) + 1 - min_header_level),

            Start(Link { dest_url, .. } | Image { dest_url, .. }) => {
                url_stack.push(dest_url.to_string());
            },
            End(TagEnd::Link) => {
                let marker = format!("[{link_ref_num}]");
                out.text(&marker);
                let url = url_stack.pop().unwrap_or_default();
                links.push((marker, url, title.to_string()));
                link_ref_num += 1;
            },
            End(TagEnd::Image) => {
                let marker = format!("[img{link_ref_num}]");
                out.text(&marker);
                let url = url_stack.pop().unwrap_or_default();
                images.push((marker, url, title.to_string()));
                link_ref_num += 1;
            },

            Start(CodeBlock(_)) => out.pre_start(),
            End(TagEnd::CodeBlock) => out.pre_end(),

            Start(List(num)) => {
                list_item_num = num;
                out.indent();
            },
            End(TagEnd::List(_)) => out.outdent(),

            Start(Item) => {
                in_list = true;
                first_para_in_list = true;
                out.list_start(list_item_num);
                if let Some(n) = &mut list_item_num {
                    *n += 1;
                }
            },
            End(TagEnd::Item) => {
                in_list = false;
                out.list_end();
                flush_links(&mut out, &mut links);
            },

            Start(BlockQuote(kind)) => {
                out.blockquote_start();
                if let Some(kind) = kind {
                    out.italic_start();
                    out.text(match kind {
                        BlockQuoteKind::Note => "Note",
                        BlockQuoteKind::Tip => "Tip",
                        BlockQuoteKind::Important => "Important",
                        BlockQuoteKind::Warning => "Warning",
                        BlockQuoteKind::Caution => "Caution",
                    });
                    out.italic_end();
                    out.line_break();
                }
            },
            End(TagEnd::BlockQuote(_)) => {
                flush_links(&mut out, &mut links);
                out.blockquote_end();
            },

            Start(Paragraph) => {
                html_state.reset();
                if in_list {
                    if first_para_in_list {
                        first_para_in_list = false;
                    } else {
                        out.empty_line();
                    }
                } else {
                    out.paragraph_start();
                }
            },
            End(TagEnd::Paragraph) => {
                flush_links(&mut out, &mut links);
                out.paragraph_end();
            },

            Start(Emphasis) => out.italic_start(),
            End(TagEnd::Emphasis) => out.italic_end(),

            Start(Strikethrough) => {
                out.text("~"); out.italic_start();
            },
            End(TagEnd::Strikethrough) => {
                out.italic_end(); out.text("~");
            },

            Start(Strong) => out.bold_start(),
            End(TagEnd::Strong) => out.bold_end(),

            Start(Superscript) => { out.text("^"); },
            End(TagEnd::Superscript) => {},
            Start(Subscript) => { out.text("_"); },
            End(TagEnd::Subscript) => {},

            HardBreak => out.line_break(),
            SoftBreak => out.ensure_line_start(),
            Code(text) => out.code(&text),
            Text(text) => out.text(&text),
            FootnoteReference(s) | Start(FootnoteDefinition(s)) => {
                out.text(&format!("[*{s}]"));
            },
            End(TagEnd::FootnoteDefinition) => {},

            Start(HtmlBlock) => out.paragraph_start(),
            End(TagEnd::HtmlBlock) => out.paragraph_end(),

            Start(MetadataBlock(_)) |
            End(TagEnd::MetadataBlock(_)) => out.ensure_line_start(),

            Start(Table(_)) => {
                html_state.reset();
                out.table_start();
            },
            End(TagEnd::Table) => out.table_end(),
            Start(TableRow) | Start(TableHead) => out.table_row_start(),
            End(TagEnd::TableRow) | End(TagEnd::TableHead) => out.table_row_end(),
            Start(TableCell) => out.table_cell_start(),
            End(TagEnd::TableCell) => out.table_cell_end(),

            Start(DefinitionList) => {
                html_state.reset();
                out.ensure_line_start();
            },
            Start(DefinitionListDefinition) => {
                out.indent();
            }
            End(TagEnd::DefinitionListDefinition) => {
                out.outdent();
            }
            End(TagEnd::DefinitionList) => {
                flush_links(&mut out, &mut links);
            },
            Start(DefinitionListTitle) => out.bold_start(),
            End(TagEnd::DefinitionListTitle) => {
                out.bold_end();
                out.ensure_line_start();
            },
        }
    }

    flush_links(&mut out, &mut links);
    flush_links(&mut out, &mut images);

    out.out
}

struct Rough {
    out: String,
    in_quotes: bool,
    unclosed_table_cell: bool,
    bold_level: u8,
    italic_level: u8,
}

impl Rough {
    pub fn title(&mut self, title: &str, man_section: u8) {
        self.ensure_line_start();
        self.out.push_str(".TH \"");
        self.in_quotes = true;
        self.text(title);
        self.in_quotes = false;
        self.text(&format!("\" {man_section}"));
        self.out.push('\n');
    }

    pub fn section_start(&mut self, level: u32) {
        self.ensure_line_start();
        self.in_quotes = true;
        // extra line needed too, otherwise headers get wrapped into prev paragraph?
        self.out.push('\n');
        match level {
            1 => self.out.push_str(".SH \""),
            2 => self.out.push_str(".SS \""),
            _ => self.out.push_str(".SB \""),
        }
    }

    pub fn section_end(&mut self, _level: u32) {
        self.in_quotes = false;
        self.out.push_str("\"\n");
    }

    pub fn table_start(&mut self) {
        self.paragraph_start();
        // self.out.push_str(".TS\n");
    }

    pub fn table_end(&mut self) {
        self.paragraph_end();
        // self.out.push_str(".TE\n");
    }

    pub fn table_row_start(&mut self) {
        self.ensure_line_start();
    }

    pub fn table_row_end(&mut self) {
        if self.unclosed_table_cell {
            self.unclosed_table_cell = false;
            self.text("\t|");
        }
        self.line_break()
    }

    pub fn table_cell_start(&mut self) {
        self.text(if self.unclosed_table_cell { "\t| " } else { "| " });
    }

    pub fn table_cell_end(&mut self) {
        self.unclosed_table_cell = true;
    }

    pub fn paragraph_start(&mut self) {
        self.ensure_line_start();
        self.out.push_str(".PP\n");
    }

    pub fn paragraph_end(&mut self) {
        debug_assert_eq!(0, self.bold_level);
        debug_assert_eq!(0, self.italic_level);
        self.out.push('\n');
    }

    pub fn blockquote_start(&mut self) {
        self.indent();
        // self.ensure_line_start();
        // self.out.push_str(".QS\n");
    }

    pub fn blockquote_end(&mut self) {
        self.outdent();
        // self.ensure_line_start();
        // self.out.push_str(".QE\n");
    }

    pub fn list_start(&mut self, n: Option<u64>) {
        self.ensure_line_start();
        self.out.push_str(".Bl\n");
        if let Some(n) = n {
            self.out.push_str(&format!(".IP {n}. 4\n"));
        } else {
            self.out.push_str(".IP \\(bu 4\n");
        }
    }

    pub fn list_end(&mut self) {
        self.ensure_line_start();
        self.out.push_str(".El\n");
    }

    pub fn ensure_line_start(&mut self) {
        if self.out.is_empty() || self.out.ends_with('\n') {
            return;
        }
        self.out.push('\n');
    }

    pub fn text(&mut self, text: &str) {
        let text = deunicode::deunicode(text);
        let text = if self.in_quotes {
            text.replace('"', "\"\"")
        } else {
            text.replace('-', "\\-").replace('.', "\\.")
        };
        self.out.push_str(&text);
    }

    pub fn code(&mut self, text: &str) {
        self.out.push_str("`\\f[CR]");
        self.text(text);
        self.out.push_str("\\fP`");
    }

    pub fn pre_start(&mut self) {
        self.indent();
        self.ensure_line_start();
        self.out.push_str(".PP\n");
        self.out.push_str(".nf\n");
    }
    pub fn pre_end(&mut self) {
        self.ensure_line_start();
        self.out.push_str(".fi\n");
        self.outdent();
    }

    pub fn line_break(&mut self) {
        self.ensure_line_start();
        self.out.push_str(".nf\n.fi\n");
    }

    pub fn empty_line(&mut self) {
        self.ensure_line_start();
        self.out.push_str(".sp\n");
    }

    pub fn italic_start(&mut self) {
        self.italic_level += 1;
        self.out.push_str("\\fI");
    }
    pub fn italic_end(&mut self) {
        self.italic_level -= 1;
        self.out.push_str("\\fP");
    }

    pub fn bold_start(&mut self) {
        self.bold_level += 1;
        self.out.push_str("\\fB");
    }
    pub fn bold_end(&mut self) {
        self.bold_level -= 1;
        self.out.push_str("\\fP");
    }

    pub fn indent(&mut self) {
        self.ensure_line_start();
        self.out.push_str(".RS\n");
    }
    pub fn outdent(&mut self) {
        self.ensure_line_start();
        self.out.push_str(".RE\n");
    }
    pub fn centered(&mut self, text: &str) {
        self.ensure_line_start();
        self.out.push_str(".ce 1000\n");
        self.text(text);
        self.ensure_line_start();
        self.out.push_str(".ce 0\n");
    }
}

struct TagStrip {
    in_comment: bool,
    in_tag: bool,
    in_arg: Option<char>,
}

impl TagStrip {
    fn reset(&mut self) {
        self.in_comment = false;
        self.in_tag = false;
        self.in_arg = None;
    }

    fn strip_tags(&mut self, txt: &str) -> String {
        let mut out = String::with_capacity(txt.len() / 2);
        let mut dashes = 0u16;
        let mut maybe_in_tag = false;
        out.extend(txt.chars().filter(move |&ch| {
            match ch {
                '>' if self.in_tag && self.in_arg.is_none() => {
                    self.in_tag = false;
                },
                '>' if self.in_comment && dashes >= 2 => {
                    self.in_comment = false;
                    dashes = 0;
                },
                '-' if self.in_comment => {
                    dashes += 1;
                }
                '<' if !self.in_tag && !maybe_in_tag => {
                    maybe_in_tag = true;
                },
                'a'..='z' | 'A'..='Z' | '/' if maybe_in_tag => {
                    maybe_in_tag = false;
                    self.in_tag = true;
                },
                '!' if !self.in_tag && maybe_in_tag => {
                    self.in_comment = true;
                    maybe_in_tag = false;
                },
                '"' | '\'' if self.in_tag && self.in_arg.is_none() => {
                    self.in_arg = Some(ch);
                },
                '"' | '\'' if self.in_arg == Some(ch) => {
                    self.in_arg = None;
                },
                _ if self.in_tag => {},
                _ => {
                    dashes = 0;
                    return true;
                },
            }
            false
        }));
        out
    }
}
